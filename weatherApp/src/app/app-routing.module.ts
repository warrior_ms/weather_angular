import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DataComponent } from './data/data.component';
import { WeatherDetailsComponent } from './weather-details/weather-details.component';


const routes: Routes = [
  {path: '', component: DataComponent, pathMatch: 'full'},
  {path: 'detail', component: WeatherDetailsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
