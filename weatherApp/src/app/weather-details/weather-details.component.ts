import { Component, OnInit } from '@angular/core';
import { GetDataService } from '../get-data.service';


@Component({
  selector: 'app-weather-details',
  templateUrl: './weather-details.component.html',
  styleUrls: ['./weather-details.component.css']
})
export class WeatherDetailsComponent implements OnInit {

  weather = {}

  constructor(private service: GetDataService) {}

  ngOnInit() {
    this.weather = this.service.getCurrentWeather();
  }

}
