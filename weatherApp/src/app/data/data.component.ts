import { Component, OnInit } from '@angular/core';
import { GetDataService } from '../get-data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {
  data: any;
  constructor(private weatherService: GetDataService, private router: Router) { }

  ngOnInit() {
    this.weatherService.getProducts().subscribe(response => {   
      this.data = response.list;
    });
  }


  showDet(obj : any) :void{
    this.weatherService.setCurrentWeather(obj.main);
    console.log(obj.main);
    this.router.navigateByUrl('/detail')
  }

}
