import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GetDataService {

  currentWeather = {}
  endpnt = 'https://samples.openweathermap.org/data/2.5/forecast/hourly?id=524901&appid=b6907d289e10d714a6e88b30761fae22';

  httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json'
      })
    };
  constructor(private http: HttpClient) { }

  getProducts(): Observable<any> {
    return this.http.get<any>('./assets/dataset.json');
  }

  getCurrentWeather() {
    return this.currentWeather;
  }

  setCurrentWeather(weather) {
    this.currentWeather = weather;
  }
}
